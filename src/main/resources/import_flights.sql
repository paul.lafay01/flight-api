BEGIN;
INSERT INTO flights(id, number, origin, destination, departure_date, departure_time, arrival_date, arrival_time, plane_id)
VALUES(NEXTVAL('flights_sequence_in_database'), '1234', 'Paris', 'Tokyo', '2023-03-10', '9h', '10mars', '18h', 1);
INSERT INTO flights(id, number, origin, destination, departure_date, departure_time, arrival_date, arrival_time, plane_id)
VALUES(NEXTVAL('flights_sequence_in_database'), '5678', 'Oslo', 'Moscou', '11avril', '22h', '12avril', '2h', 2);
COMMIT;

BEGIN;
INSERT INTO planes(id, operator, model, registration, capacity)
VALUES(NEXTVAL('planes_sequence_in_database'), 'AirbusIndustrie', 'AIRBUS A380', 'F-ABCD', 3);
INSERT INTO planes(id, operator, model, registration, capacity)
VALUES(NEXTVAL('planes_sequence_in_database'), 'Boeing', 'BOEING CV2', 'F-AZER', 3);
COMMIT;

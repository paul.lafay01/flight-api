BEGIN;
INSERT INTO passengers(id, surname, firstname, email_address)
VALUES(NEXTVAL('passengers_sequence_in_database'), 'LAFAY', 'Paul', 'paul.lafay@etu.unilasalle.fr');
INSERT INTO passengers(id, surname, firstname, email_address)
VALUES(NEXTVAL('passengers_sequence_in_database'), 'LAFAY', 'Zoé', 'zoe.lafay59@gmail.com');
COMMIT;

package fr.unilasalle.flight.api.exceptions;

public class ParamNotFoundException extends Exception {
    public ParamNotFoundException() { super(); }
    public ParamNotFoundException(String s) { super(s); }
}

package fr.unilasalle.flight.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "reservations")
public class Reservation extends PanacheEntityBase {

    @Id
    @SequenceGenerator(name = "reservations_sequence_in_java_code", sequenceName = "reservations_sequence_in_database", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservations_sequence_in_java_code")
    private Long id;

    // @NotBlank(message = "Flight Id cannot be null")
    // @Column(nullable = false)
    // private Long flight_id;

    @ManyToOne
    @JoinColumn(name = "flight_id", referencedColumnName = "id", nullable = false)
    @JsonIgnoreProperties({"number", "origin", "destination", "departure_date", "departure_time", "arrival_date", "arrival_time", "plane"})
    @NotNull(message = "Flight Id cannot be null")
    private Flight flight;

    // @NotBlank(message = "Passenger Id cannot be null")
    // @Column(nullable = false)
    // private Long passenger_id;

    @ManyToOne
    @JoinColumn(name = "passenger_id", referencedColumnName = "id", nullable = false)
    //@JsonIgnoreProperties({"surname", "firstname", "email_address"})
    @NotNull(message = "Passenger Id cannot be null")
    private Passenger passenger;

}

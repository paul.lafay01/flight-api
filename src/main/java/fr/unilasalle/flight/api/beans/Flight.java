package fr.unilasalle.flight.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "flights")
public class Flight extends PanacheEntityBase {

    @Id
    @SequenceGenerator(name = "flights_sequence_in_java_code", sequenceName = "flights_sequence_in_database", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "flights_sequence_in_java_code")
    private Long id;

    @NotBlank(message = "Number cannot be null")
    @Column(nullable = false, unique = true)
    private String number;

    @NotBlank(message = "Origin cannot be null")
    @Column(nullable = false)
    private String origin;

    @NotBlank(message = "Destination cannot be null")
    @Column(nullable = false)
    private String destination;

    @NotBlank(message = "Departure date cannot be null")
    @Column(nullable = false)
    private String departure_date;

    @NotBlank(message = "Departure time cannot be null")
    @Column(nullable = false)
    private String departure_time;

    @NotBlank(message = "Arrival date cannot be null")
    @Column(nullable = false)
    private String arrival_date;

    @NotBlank(message = "Arrival time cannot be null")
    @Column(nullable = false)
    private String arrival_time;

    // @NotBlank(message = "Plane Id cannot be null")
    // @Column(nullable = false)
    // private Long plane_id;

    @OneToOne
    @JoinColumn(name = "plane_id", referencedColumnName = "id", nullable = false)
    @JsonIgnoreProperties({"operator", "model", "registration", "capacity"})
    @NotNull(message = "Plane Id cannot be null")
    private Plane plane;

}

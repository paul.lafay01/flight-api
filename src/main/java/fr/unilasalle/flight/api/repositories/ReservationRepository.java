package fr.unilasalle.flight.api.repositories;

import fr.unilasalle.flight.api.beans.Flight;
import fr.unilasalle.flight.api.beans.Reservation;
import fr.unilasalle.flight.api.exceptions.ParamNotFoundException;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.inject.Model;

import java.util.List;

@Model
public class ReservationRepository implements PanacheRepositoryBase<Reservation, Long> {

    public List<Reservation> findByFlight(Flight flight) throws ParamNotFoundException {
        List<Reservation> list = find("flight", flight).list();
        if(list.isEmpty()) throw new ParamNotFoundException();
        return list;
    }

}

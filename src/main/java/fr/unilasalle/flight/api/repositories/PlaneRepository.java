package fr.unilasalle.flight.api.repositories;

import fr.unilasalle.flight.api.beans.Plane;
import fr.unilasalle.flight.api.exceptions.ParamNotFoundException;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.inject.Model;

import java.util.List;

@Model
public class PlaneRepository implements PanacheRepositoryBase<Plane, Long> {

    public List<Plane> findByOperator(String operatorParameter) throws ParamNotFoundException {
        return find("operator", operatorParameter).list();
    }

}

package fr.unilasalle.flight.api.repositories;

import fr.unilasalle.flight.api.beans.Passenger;
import fr.unilasalle.flight.api.exceptions.ParamNotFoundException;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.inject.Model;

import java.util.List;

@Model
public class PassengerRepository implements PanacheRepositoryBase<Passenger, Long> {

    public Passenger findByEmailAddress(String emailAddressParameter) throws ParamNotFoundException {
        List<Passenger> list;
        try {
            list = find("email_address", emailAddressParameter).list();
            return list.get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new ParamNotFoundException("Email introuvable");
        }
    }

}

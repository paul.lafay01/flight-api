package fr.unilasalle.flight.api.resources;

import fr.unilasalle.flight.api.beans.Flight;
import fr.unilasalle.flight.api.beans.Reservation;
import fr.unilasalle.flight.api.exceptions.ParamNotFoundException;
import fr.unilasalle.flight.api.repositories.FlightRepository;
import fr.unilasalle.flight.api.repositories.ReservationRepository;
import jakarta.inject.Inject;
import jakarta.persistence.PersistenceException;
import jakarta.transaction.Transactional;
import jakarta.validation.Validator;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Path("/flights")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FlightResource extends GenericResource{

    @Inject
    private FlightRepository repository;

    @Inject
    private ReservationRepository reservationRepository;

    @Inject
    Validator validator;

    @GET
    public Response getFlights(@QueryParam("destination") String destination) {
        List<Flight> list;
        if(StringUtils.isBlank(destination)) list = repository.listAll();
        else {
            try {
                list = repository.findByDestination(destination);
            } catch (ParamNotFoundException e) {
                return Response.status(404).entity(new ErrorWrapper("Destination inconnue")).build();
            }
        }
        return getOr404(list);
    }

    @GET
    @Path("/{id}")
    public Response getFlight(@PathParam("id") Long id) {
        var list = repository.findByIdOptional(id).orElse(null);
        return getOr404(list);
    }

    @POST
    @Transactional
    public Response createFlight(Flight flight) {
        var violations = validator.validate(flight);
        if(!violations.isEmpty()) return Response.status(400).entity(new ErrorWrapper(violations)).build();
        try {
            repository.persistAndFlush(flight);
            return Response.status(201).build();
        } catch (PersistenceException e) {
            return Response.serverError().entity(new ErrorWrapper(e.getMessage())).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response deleteFlight(@PathParam("id") Long id) {
        try {
            Flight flightToDelete;
            List<Reservation> reservationsToDelete;
            flightToDelete = repository.findById(id);
            reservationsToDelete = reservationRepository.findByFlight(flightToDelete);
            for (Reservation reservation: reservationsToDelete) { reservation.delete(); }
            flightToDelete.delete();
            return Response.status(201).build();
        } catch (NotFoundException | ParamNotFoundException e) {
            return Response.status(404).entity(new ErrorWrapper("Flight id inconnu")).build();
        }
    }

}

package fr.unilasalle.flight.api.resources;

import fr.unilasalle.flight.api.beans.Passenger;
import fr.unilasalle.flight.api.exceptions.ParamNotFoundException;
import fr.unilasalle.flight.api.repositories.PassengerRepository;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.Validator;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Path("/passengers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PassengerResource extends GenericResource{

    @Inject
    PassengerRepository repository;

    @Inject
    Validator validator;

    @GET
    public Response getPassenger(@QueryParam("email_address") String email_address) {
        if(StringUtils.isBlank(email_address)) {
            List<Passenger> list = repository.listAll();
            return getOr404(list);
        } else {
            try {
                Passenger passenger = repository.findByEmailAddress(email_address);
                return getOr404(passenger);
            } catch (ParamNotFoundException e) {
                return Response.status(400).entity(new ErrorWrapper("Cette adresse email n'existe pas !")).build();
            }
        }
    }

    @GET
    @Path("/{id}")
    public Response getPassenger(@PathParam("id") Long id) {
        var list = repository.findByIdOptional(id).orElse(null);
        return getOr404(list);
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response modifyPassenger(@PathParam("id") Long id, Passenger passenger) {
        var violations = validator.validate(passenger);
        if(!violations.isEmpty()) return Response.status(400).entity(new ErrorWrapper(violations)).build();
        try {
            Passenger modifiedPassenger = repository.findById(id);
            modifiedPassenger.setFirstname(passenger.getFirstname());
            modifiedPassenger.setSurname(passenger.getSurname());
            modifiedPassenger.persist();
            return Response.status(201).build();
        } catch (NotFoundException e) {
            return Response.serverError().entity(new ErrorWrapper(e.getMessage())).build();
        }
    }

}

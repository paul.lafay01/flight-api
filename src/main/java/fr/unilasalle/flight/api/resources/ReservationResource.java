package fr.unilasalle.flight.api.resources;

import fr.unilasalle.flight.api.beans.*;
import fr.unilasalle.flight.api.exceptions.ParamNotFoundException;
import fr.unilasalle.flight.api.repositories.*;
import jakarta.inject.Inject;
import jakarta.persistence.PersistenceException;
import jakarta.transaction.Transactional;
import jakarta.validation.Validator;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Path("/reservations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReservationResource extends GenericResource{

    @Inject
    private ReservationRepository repository;

    @Inject
    PassengerRepository passengerRepository;

    @Inject
    FlightRepository flightRepository;

    @Inject
    PlaneRepository planeRepository;

    @Inject
    Validator validator;

    @GET
    public Response getReservations(@QueryParam("flight") String flightNumber) {
        List<Reservation> list;
        if(StringUtils.isBlank(flightNumber)) list = repository.listAll();
        else {
            try {
                Flight flight = flightRepository.findByNumber(flightNumber).get(0);
                list = repository.findByFlight(flight);
            } catch (ParamNotFoundException e) {
                return Response.status(404).entity(new ErrorWrapper("Flight number inconnu")).build();
            }
        }
        return getOr404(list);
    }

    @POST
    @Transactional
    public Response createReservation(Reservation reservation) {
        var violations = validator.validate(reservation);
        if(!violations.isEmpty()) return Response.status(400).entity(new ErrorWrapper(violations)).build();

        Flight flight = flightRepository.findById(reservation.getFlight().getId());

        try {
            Plane plane = planeRepository.findById(flight.getPlane().getId());
            int capacity = plane.getCapacity();
            int reservations = repository.findByFlight(flight).size();
            System.out.println(reservations);
            if(reservations >= capacity) return Response.status(403).entity(new ErrorWrapper("L'avion est plein !")).build();
        } catch (ParamNotFoundException e) {
            return Response.status(404).entity(new ErrorWrapper("Flight id inconnu")).build();
        }

        Passenger passenger;
        try {
            passenger = passengerRepository.findByEmailAddress(reservation.getPassenger().getEmail_address());
        } catch (ParamNotFoundException e1) {
            passenger = new Passenger();
            passenger.setEmail_address(reservation.getPassenger().getEmail_address());
            passenger.setFirstname(reservation.getPassenger().getFirstname());
            passenger.setSurname(reservation.getPassenger().getSurname());
            try {
                passenger.persistAndFlush();
            } catch (PersistenceException e2) {
                return Response.serverError().entity(new ErrorWrapper(e2.getMessage())).build();
            }
        }

        reservation.setFlight(flight);
        reservation.setPassenger(passenger);

        try {
            repository.persistAndFlush(reservation);
            return Response.status(201).build();
        } catch (PersistenceException e) {
            return Response.serverError().entity(new ErrorWrapper(e.getMessage())).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response deleteReservation(@PathParam("id") Long id) {
        try {
            Reservation reservation = repository.findById(id);
            reservation.delete();
            return Response.status(201).build();
        } catch (NotFoundException e) {
            return Response.serverError().entity(new ErrorWrapper(e.getMessage())).build();
        }
    }

}
